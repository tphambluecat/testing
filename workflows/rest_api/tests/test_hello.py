from rest_api.hello import HelloModule
import unittest


class HelloTests(unittest.TestCase):
    def test_main(self):
        hel = HelloModule()
        hel.foo("hello world")
        self.assertEqual(hel.msg, "HELLO WORLD")

    def test_main1(self):
        hel = HelloModule()
        hel.foo1("hello world")
        self.assertEqual(hel.msg1, "HELLO WORLD")

