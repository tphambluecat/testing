
class HelloModule():
    def foo(self, msg):
        self.msg= msg.upper()
        self.bar()

    def bar(self):
        print(self.msg)

    def foo1(self, msg):
        self.msg1= msg.upper()


def main():
    print("hello, I'm main")
    return 0


if __name__ == '__main__':
    raise SystemExit(main())